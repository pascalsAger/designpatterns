class UndefinedShape(Exception):
	pass

#Abstract Shape interface: which shape does what.Here we have two because we classify two kinds of shapes that we are interested in.
class Shape2DInterface:
	def draw(self): pass

class Shape3DInterface:
	def build(self): pass


#These are concrete shapes


class Circle(Shape2DInterface):
	def draw(self):
		print('Drawing Circle')

class Sphere(Shape3DInterface):
	def build(self):
		print('Building Sphere')

class Square(Shape2DInterface):
	def draw(self):
		print('Drawing Square')

class Cube(Shape3DInterface):
	def build(self):
		print('Building Cube')



#Abstract Factory

class ShapeFactoryInterface:
	def getShape(sides): pass


class Shape2DFactory(ShapeFactoryInterface):
	@staticmethod
	def getShape(sides):
		if sides==1:
			return Circle()
		if sides==4:
			return Square()
		raise UndefinedShape('Undefined 2D Shape with '+sides)

class Shape3DFactory(ShapeFactoryInterface):
	@staticmethod
	def getShape(sides):
		if sides==1:
			return Sphere()
		if sides==6:
			return Cube()
		raise UndefinedShape('Undefined 3D Shape with '+sides)		



'''
>>> from abstractfactory import Shape2DFactory, Shape3DFactory
>>> s2 = Shape2DFactory()
>>> s2.getShape(1)
<abstractfactory.Circle object at 0x7f2379165cf8>
>>> s2.draw()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'Shape2DFactory' object has no attribute 'draw'
>>> s2.getShape(1).draw()
Drawing Circle
'''