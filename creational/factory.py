class UndefinedShape(Exception):
	pass

class ShapeInterface:
	def draw(self):
		pass


class Circle(ShapeInterface):
	def draw(self):
		print('Drawing Circle')


class Square(ShapeInterface):
	def draw(self):
		print('Drawing Square')



class ShapeFactory:
	@staticmethod
	def getShape(type):
		if type == 'Circle' or type == 'circle':
			return Circle()
		if type == 'Square' or type == 'square':
			return Square()
		raise UndefinedShape(type + ' is an undefined shape')



'''
>>> from factory import ShapeFactory
>>> f = ShapeFactory()
>>> square = f.getShape('square')
>>> square
<factory.Square object at 0x7fc301773da0>
>>> circle = f.getShape('circle')
>>> circle
<factory.Circle object at 0x7fc2ff5bdd30>
>>> triangle = f.getShape('triangle')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/monty/designpatterns/creational/factory.py", line 27, in getShape
    raise UndefinedShape(type + ' is an undefined shape')
factory.UndefinedShape: triangle is an undefined shape
'''