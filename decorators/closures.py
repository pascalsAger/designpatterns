def out_function(msg):
	def inner_function():
		print(msg)
	return inner_function

hi_fun = out_function('hi')

bye_fun = out_function('bye')

hi_fun()
bye_fun()