class decorator_class:
	def __init__(self, original_function):
		self.original_function = original_function

	def __call__(self, *args, **kwargs):
		print('call method executed before {}'.format(self.original_function.__name__))
		return self.original_function(*args, **kwargs)



@decorator_class
def myCustomFunction():
	print('running custom function')

@decorator_class
def mySecondCustomFunction(name, age):
	print('running second custom function with arguments ({} and {})'.format(name, age))


myCustomFunction()

mySecondCustomFunction('Advith', 23)