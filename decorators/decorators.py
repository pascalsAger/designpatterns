def decorator_function(original_function):
	def wrapper_function():
		print('wrapper executed before {}'.format(original_function.__name__))
		original_function()
	return wrapper_function


def myCustomFunction():
	print('running custom function')


decorated_custom = decorator_function(myCustomFunction)

decorated_custom()

#add