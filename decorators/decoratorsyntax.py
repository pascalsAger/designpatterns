def decorator_function(original_function):
	def wrapper_function(*args, **kwargs):
		print('wrapper executed before {}'.format(original_function.__name__))
		original_function(*args, **kwargs)
	return wrapper_function

@decorator_function
def myCustomFunction():
	print('running custom function')

@decorator_function
def mySecondCustomFunction(name, age):
	print('running second custom function with arguments ({} and {})'.format(name, age))


myCustomFunction()

mySecondCustomFunction('Advith', 23)

#add